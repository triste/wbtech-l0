include deploy/.env

DOCKER_COMPOSE_FILE ?= deploy/docker-compose.yml

build:
	docker-compose -f ${DOCKER_COMPOSE_FILE} build

run:
	docker-compose -f ${DOCKER_COMPOSE_FILE} up

psql:
	docker compose -f ${DOCKER_COMPOSE_FILE} exec postgres \
		psql -U ${POSTGRES_USER} -d ${POSTGRES_DB}

migrate_up:
	docker compose -f ${DOCKER_COMPOSE_FILE} --profile tools run --rm \
		migrate up 1

migrate_down:
	docker compose -f ${DOCKER_COMPOSE_FILE} --profile tools run --rm \
		migrate down 1

.PHONY: run psql migrate_up migrate_down build

package main

import "gitlab.com/triste/wbtech-l0/internal/app"

type config struct {
	Nats    app.NatsConfig `envPrefix:"NATS_"`
	Subject string         `env:"NATSPUB_SUBJECT"`
	Rate    uint           `env:"NATSPUB_RATE"`
}

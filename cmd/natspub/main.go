package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"strings"
	"time"
	"unicode"

	"github.com/caarlos0/env/v11"
	"github.com/nats-io/stan.go"
	"gitlab.com/triste/wbtech-l0/internal/models"
	"go.uber.org/zap"
)

func main() {
	cfg, err := env.ParseAs[config]()
	logger, err := zap.NewDevelopment()
	if err != nil {
		log.Fatalln(err)
	}
	defer logger.Sync()
	url := fmt.Sprintf("nats://%v:%v", cfg.Nats.Host, cfg.Nats.Port)
	conn, err := stan.Connect(cfg.Nats.Cluster, cfg.Nats.Client, stan.NatsURL(url))
	defer conn.Close()
	if err != nil {
		log.Fatalln(err)
	}
	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)
	defer stop()
	tickCh := time.Tick(time.Second / time.Duration(cfg.Rate))
	for {
		select {
		case <-ctx.Done():
			return
		case <-tickCh:
			order := generateOrder()
			corrupted := false
			if rand.Intn(3) == 0 {
				corrupted = true
				corruptOrder(&order)
			}
			data, err := json.Marshal(order)
			if err != nil {
				logger.Warn("order marshal failed",
					zap.Any("order", order),
					zap.Error(err),
				)
				continue
			}
			logger.Debug("sending order",
				zap.String("id", order.ID),
				zap.Bool("corrupted", corrupted),
			)
			if err := conn.Publish(cfg.Subject, data); err != nil {
				logger.Warn("order publish failed",
					zap.Any("order", order),
					zap.String("subject", cfg.Subject),
					zap.Error(err),
				)
			}
		}
	}
}

func generateString(letters string, min, max int, pascalCase bool) string {
	if min > max {
		return ""
	}
	lenght := min
	if min != max {
		lenght += rand.Intn(max - min)
	}
	if lenght == 0 {
		return ""
	}
	out := make([]rune, lenght)
	runes := []rune(letters)
	for i := range out {
		out[i] = runes[rand.Intn(len(runes))]
	}
	if pascalCase {
		out[0] = unicode.ToUpper(out[0])
	}
	return string(out)
}

func generateOrder() models.Order {
	digits := "01234569"
	lower := "abcdefghijklmnopqrstuvwxyz"
	upper := strings.ToUpper(lower)
	trackNumber := "WBIL" + generateString(upper, 4, 12, false)
	return models.Order{
		ID:          generateString(lower+digits, 19, 19, false),
		TrackNumber: trackNumber,
		Entry:       "WBIL",
		Delivery: models.Delivery{
			Name: strings.Join([]string{
				generateString(lower, 4, 12, true),
				generateString(lower, 4, 12, true),
			}, " "),
			Phone: "+9" + generateString(digits, 9, 9, false),
			Zip:   generateString(digits, 7, 7, false),
			City:  generateString(lower, 4, 20, true),
			Address: strings.Join([]string{
				generateString(lower, 4, 12, true),
				generateString(lower, 4, 12, true),
				generateString(digits, 1, 3, false),
			}, ""),
			Region: generateString(lower, 4, 20, true),
			Email:  generateString(lower, 1, 32, false) + "@gmail.com",
		},
		Payment: models.Payment{
			Transaction:  generateString(lower+digits, 19, 19, false),
			RequestID:    "",
			Currency:     "USD",
			Provider:     "wbpay",
			Amount:       rand.Intn(1817),
			PaymentDt:    rand.Intn(1637907727),
			Bank:         "alpha",
			DeliveryCost: rand.Intn(1500),
			GoodsTotal:   rand.Intn(317),
			CustomFee:    0,
		},
		Items: []models.Item{
			{
				ChrtID:      rand.Intn(9934930),
				TrackNumber: trackNumber,
				Price:       rand.Intn(453),
				Rid:         generateString(lower+digits, 19, 19, false),
				Name:        generateString(lower, 4, 12, true),
				Sale:        rand.Intn(100),
				Size:        generateString(digits, 1, 3, false),
				TotalPrice:  rand.Intn(317),
				NmID:        rand.Intn(2389212),
				Brand: strings.Join([]string{
					generateString(lower, 4, 12, true),
					generateString(lower, 4, 12, true),
				}, " "),
				Status: 202,
			},
		},
		Locale:            "en",
		InternalSignature: "",
		CustomerID:        generateString(lower, 4, 8, false),
		DeliveryService:   "meest",
		Shardkey:          "9",
		SmID:              99,
		DateCreated:       time.Now(),
		OofShard:          "1",
	}
}

func corruptOrder(order *models.Order) {
	whatCorrupt := rand.Intn(5)
	switch whatCorrupt {
	case 0:
		order.ID = ""
	case 1:
		order.Items = nil
	case 2:
		order.Locale = "corrupted"
	case 3:
		order.DateCreated = time.Time{}
	case 4:
		order.Delivery.Phone = "corrupted"
	}
}

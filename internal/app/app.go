package app

import (
	"context"
	"errors"

	cacheInmem "gitlab.com/triste/wbtech-l0/internal/cache/inmem"
	repoPg "gitlab.com/triste/wbtech-l0/internal/repo/postgres"
	srv "gitlab.com/triste/wbtech-l0/internal/service"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

func Run(ctx context.Context, cfg Config) (retErr error) {
	logger, err := zap.NewDevelopment()
	if err != nil {
		return err
	}
	defer logger.Sync()

	// Setup Postgres connection
	postgres := newPostgres(logger, cfg.Postgres)
	if err := postgres.Open(); err != nil {
		return err
	}
	defer func() {
		err := postgres.Close()
		retErr = errors.Join(retErr, err)
	}()

	// Initialize service with cached storage
	orderRepo := repoPg.NewOrderRepository(logger, postgres.db)
	orderCache, err := cacheInmem.NewOrderCache(orderRepo)
	if err != nil {
		return err
	}
	service := srv.NewService(logger, orderRepo, orderCache)

	// Connect to NATS and subscribe to a subject
	natsClient := newNatsClient(logger, cfg.Nats)
	if err := natsClient.Open(); err != nil {
		return err
	}
	defer func() {
		err := natsClient.Close()
		retErr = errors.Join(retErr, err)
	}()
	natsSub, err := natsClient.Subscribe(cfg.NatsSub, &service)
	if err != nil {
		return err
	}
	defer func() {
		err := natsSub.Close()
		retErr = errors.Join(retErr, err)
	}()

	var g errgroup.Group
	// Start HTTP server
	httpServer := newHttpServer(logger, cfg.HttpServer, &service)
	g.Go(httpServer.Run)
	<-ctx.Done()
	if err := httpServer.Stop(); err != nil {
		retErr = err
	}
	return g.Wait()
}

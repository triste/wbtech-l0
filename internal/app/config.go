package app

type Config struct {
	Nats       NatsConfig       `envPrefix:"NATS_"`
	NatsSub    NatsSubConfig    `envPrefix:"NATSSUB_"`
	Postgres   PostgresConfig   `envPrefix:"POSTGRES_"`
	HttpServer HttpServerConfig `envPrefix:"HTTP_SERVER_"`
}

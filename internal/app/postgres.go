package app

import (
	"database/sql"
	"fmt"
	"net/url"

	"go.uber.org/zap"
)

type PostgresConfig struct {
	User     string `env:"USER,required"`
	Password string `env:"PASSWORD,required"`
	Host     string `env:"HOST,required"`
	Port     uint   `env:"PORT,required"`
	Db       string `env:"DB,required"`
}

type postgres struct {
	logger *zap.Logger
	url    string
	db     *sql.DB
}

func newPostgres(logger *zap.Logger, cfg PostgresConfig) *postgres {
	pgUrl := url.URL{
		Scheme:   "postgres",
		User:     url.UserPassword(cfg.User, cfg.Password),
		Host:     fmt.Sprintf("%v:%v", cfg.Host, cfg.Port),
		Path:     cfg.Db,
		RawQuery: "sslmode=disable",
	}
	pgUrlStr := pgUrl.String()
	pgLogger := logger.With(
		zap.String("url", pgUrlStr),
	)
	return &postgres{
		logger: pgLogger,
		url:    pgUrlStr,
	}
}

func (p *postgres) Open() error {
	db, err := sql.Open("postgres", p.url)
	if err != nil {
		return err
	}
	if err := db.Ping(); err != nil {
		return err
	}
	p.db = db
	p.logger.Info("connected to Postgres")
	return nil
}

func (p *postgres) Close() error {
	if err := p.db.Close(); err != nil {
		return err
	}
	p.logger.Info("closed Postgres connection")
	return nil
}

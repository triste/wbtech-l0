package app

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"

	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"gitlab.com/triste/wbtech-l0/internal/controller/rest"
	"gitlab.com/triste/wbtech-l0/internal/service"
	"go.uber.org/zap"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	_ "gitlab.com/triste/wbtech-l0/docs"
)

type HttpServerConfig struct {
	Port uint16 `env:"PORT"`
}

type httpServer struct {
	logger *zap.Logger
	srv    *http.Server
}

func newHttpServer(
	logger *zap.Logger,
	cfg HttpServerConfig,
	service *service.Service,
) *httpServer {
	ctrl := rest.NewController(service)
	router := gin.New()
	router.Use(ginzap.Ginzap(logger, time.RFC3339, true))
	router.Use(ginzap.RecoveryWithZap(logger, true))
	router.GET("/api/v1/orders/:id", ctrl.GetOrder)
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	serverLogger := logger.With(
		zap.Uint16("port", cfg.Port),
	)
	server := &http.Server{
		Addr:    fmt.Sprintf(":%v", cfg.Port),
		Handler: router.Handler(),
	}
	return &httpServer{
		logger: serverLogger,
		srv:    server,
	}
}

func (h *httpServer) Run() error {
	h.logger.Info("starting HTTP server")
	if err := h.srv.ListenAndServe(); err != nil {
		if errors.Is(err, http.ErrServerClosed) {
			h.logger.Info("closed HTTP server")
		} else {
			return err
		}
	}
	return nil
}

func (h *httpServer) Stop() error {
	err := h.srv.Shutdown(context.TODO())
	return err
}

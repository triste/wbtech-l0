package app

import (
	"fmt"
	"net/url"

	"github.com/nats-io/stan.go"
	"gitlab.com/triste/wbtech-l0/internal/controller/nats"
	"gitlab.com/triste/wbtech-l0/internal/service"
	"go.uber.org/zap"
)

type NatsConfig struct {
	Host    string `env:"HOST"`
	Port    string `env:"PORT"`
	Cluster string `env:"CLUSTER"`
	Client  string `env:"CLIENT"`
}

type natsClient struct {
	logger  *zap.Logger
	url     string
	cluster string
	name    string
	conn    stan.Conn
}

func newNatsClient(logger *zap.Logger, cfg NatsConfig) *natsClient {
	natsUrl := url.URL{
		Scheme: "nats",
		Host:   fmt.Sprintf("%v:%v", cfg.Host, cfg.Port),
	}
	natsUrlStr := natsUrl.String()
	natsLogger := logger.With(
		zap.String("url", natsUrlStr),
		zap.String("cluster", cfg.Cluster),
		zap.String("client", cfg.Client),
	)
	return &natsClient{
		logger:  natsLogger,
		url:     natsUrlStr,
		cluster: cfg.Cluster,
		name:    cfg.Client,
	}
}

func (n *natsClient) Open() error {
	opts := []stan.Option{
		stan.NatsURL(n.url),
	}
	conn, err := stan.Connect(n.cluster, n.name, opts...)
	if err != nil {
		return err
	}
	n.conn = conn
	n.logger.Info("connected to NATS")
	return nil
}

func (n *natsClient) Close() error {
	if err := n.conn.Close(); err != nil {
		return err
	}
	n.logger.Info("closed NATS connection")
	return nil
}

type natsSubscriber struct {
	logger *zap.Logger
	sub    stan.Subscription
}

type NatsSubConfig struct {
	Subject string `env:"SUBJECT"`
	Durable string `env:"DURABLE"`
}

func (n *natsClient) Subscribe(
	cfg NatsSubConfig,
	srv *service.Service,
) (*natsSubscriber, error) {
	subLogger := n.logger.With(
		zap.String("subject", cfg.Subject),
		zap.String("durable", cfg.Durable),
	)
	ctrl := nats.NewController(subLogger, srv)
	subOpts := []stan.SubscriptionOption{
		stan.DurableName(cfg.Durable),
		stan.StartWithLastReceived(),
	}
	sub, err := n.conn.Subscribe(cfg.Subject, func(msg *stan.Msg) {
		defer msg.Ack()
		if err := ctrl.OnOrderCreated(msg.Data); err != nil {
			subLogger.Debug("message handling failed",
				zap.Error(err),
			)
		}
	}, subOpts...)
	if err != nil {
		return nil, err
	}
	subLogger.Info("subscribed to NATS subject")
	return &natsSubscriber{
		logger: subLogger,
		sub:    sub,
	}, nil
}

func (s *natsSubscriber) Close() error {
	if err := s.sub.Close(); err != nil {
		return err
	}
	s.logger.Info("closed NATS subscription")
	return nil
}

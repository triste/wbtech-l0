package cache

import (
	"context"

	"gitlab.com/triste/wbtech-l0/internal/models"
)

type OrderCache interface {
	GetByID(ctx context.Context, id string) (*models.Order, error)
	Add(ctx context.Context, order *models.Order) error
}

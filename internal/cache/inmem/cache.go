package inmem

import (
	"context"
	"sync"

	"gitlab.com/triste/wbtech-l0/internal/errs"
	"gitlab.com/triste/wbtech-l0/internal/models"
	"gitlab.com/triste/wbtech-l0/internal/repo"
)

type OrderCache struct {
	orders map[string]models.Order
	mu     sync.RWMutex
}

func NewOrderCache(orderRepo repo.OrderRepository) (*OrderCache, error) {
	orderMap := make(map[string]models.Order)
	orders, err := orderRepo.GetAll(context.Background())
	if err != nil {
		return nil, err
	}
	for _, order := range orders {
		orderMap[order.ID] = order
	}
	return &OrderCache{
		orders: orderMap,
	}, nil
}

func (c *OrderCache) GetByID(ctx context.Context, id string) (*models.Order, error) {
	c.mu.RLock()
	defer c.mu.RUnlock()
	if order, ok := c.orders[id]; ok {
		return &order, nil
	}
	return nil, errs.NewNotFoundError("order with id = %v", id)
}

func (c *OrderCache) Add(ctx context.Context, order *models.Order) error {
	c.mu.Lock()
	defer c.mu.Unlock()
	if _, ok := c.orders[order.ID]; ok {
		return errs.NewAlreadyExistsError("order with id %v", order.ID)
	}
	c.orders[order.ID] = *order
	return nil
}

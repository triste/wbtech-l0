package nats

import (
	"context"
	"encoding/json"

	"gitlab.com/triste/wbtech-l0/internal/models"
	"go.uber.org/zap"
)

type Controller struct {
	logger  *zap.Logger
	service Service
}

func (c *Controller) OnOrderCreated(message []byte) error {
	var order models.Order
	if err := json.Unmarshal(message, &order); err != nil {
		c.logger.Warn("invalid order recieved",
			zap.Binary("data", message),
		)
		return &MessageUnmarshalError{
			message: "order unmarshal",
			inner:   err,
		}
	}
	if err := c.service.AddOrder(context.Background(), &order); err != nil {
		return err
	}
	c.logger.Debug("consumed order",
		zap.String("id", order.ID),
	)
	return nil
}

func NewController(logger *zap.Logger, service Service) Controller {
	return Controller{
		logger:  logger,
		service: service,
	}
}

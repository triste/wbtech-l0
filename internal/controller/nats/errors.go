package nats

import "fmt"

type MessageUnmarshalError struct {
	message string
	inner   error
}

func (e *MessageUnmarshalError) Error() string {
	return fmt.Sprintf("%v: %v", e.message, e.inner.Error())
}

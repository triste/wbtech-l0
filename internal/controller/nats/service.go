package nats

import (
	"context"

	"gitlab.com/triste/wbtech-l0/internal/models"
)

type Service interface {
	AddOrder(ctx context.Context, order *models.Order) error
}

package rest

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/triste/wbtech-l0/internal/errs"
)

// @title Order subscriber
// @version 1.0
// @description Order subscriber service
// @BasePath /api/v1
type Controller struct {
	service Service
}

func NewController(service Service) Controller {
	return Controller{
		service: service,
	}
}

// GetOrder godoc
// @Summary      Get an order
// @Description  Get order by ID
// @Tags         orders
// @Produce      json
// @Param        id   path      string  true  "Order ID"
// @Success      200  {object}  models.Order
// @Failure      404  {object}  ResponseError
// @Failure      500  {object}  ResponseError
// @Router       /orders/{id} [get]
func (c *Controller) GetOrder(gctx *gin.Context) {
	id := gctx.Param("id")
	order, err := c.service.GetOrder(gctx.Request.Context(), id)
	if err != nil {
		var code int
		switch err.(type) {
		case *errs.NotFoundError:
			code = http.StatusNotFound
		default:
			code = http.StatusInternalServerError
		}
		NewResponseError(gctx, code, err)
		return
	}
	gctx.JSON(http.StatusOK, order)
}

type ResponseError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func NewResponseError(gctx *gin.Context, status int, err error) {
	gctx.JSON(status, ResponseError{
		Code:    status,
		Message: err.Error(),
	})
}

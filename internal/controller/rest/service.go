package rest

import (
	"context"

	"gitlab.com/triste/wbtech-l0/internal/models"
)

type Service interface {
	GetOrder(ctx context.Context, id string) (*models.Order, error)
}

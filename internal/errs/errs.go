package errs

import "fmt"

type NotFoundError struct {
	message string
}

func (e *NotFoundError) Error() string {
	return e.message
}

func NewNotFoundError(format string, a ...any) *NotFoundError {
	what := fmt.Sprintf(format, a...)
	return &NotFoundError{
		message: what + " not found",
	}
}

type AlreadyExistsError struct {
	message string
}

func (e *AlreadyExistsError) Error() string {
	return e.message
}

func NewAlreadyExistsError(format string, a ...any) error {
	what := fmt.Sprintf(format, a...)
	return &AlreadyExistsError{
		message: what + " already exists",
	}
}

type InvalidOrderError struct {
	message string
}

func (e *InvalidOrderError) Error() string {
	return e.message
}

func NewInvalidOrderError(what string) *InvalidOrderError {
	message := fmt.Sprintf("invalid order: %v", what)
	return &InvalidOrderError{
		message: message,
	}
}

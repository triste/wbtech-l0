package models

import (
	"gitlab.com/triste/wbtech-l0/internal/errs"
)

type Delivery struct {
	Name    string `json:"name"`
	Phone   string `json:"phone"`
	Zip     string `json:"zip"`
	City    string `json:"city"`
	Address string `json:"address"`
	Region  string `json:"region"`
	Email   string `json:"email" validator:"email"`
}

func (d *Delivery) Validate() error {
	if len(d.Phone) != 11 || d.Phone[0] != '+' || d.Phone[1] != '9' {
		return errs.NewInvalidOrderError("phone number")
	}
	return nil
}

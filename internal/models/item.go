package models

import (
	"gitlab.com/triste/wbtech-l0/internal/errs"
)

type Item struct {
	ChrtID      int    `json:"chrt_id"`
	TrackNumber string `json:"track_number"`
	Price       int    `json:"price"`
	Rid         string `json:"rid"`
	Name        string `json:"name"`
	Sale        int    `json:"sale"`
	Size        string `json:"size"`
	TotalPrice  int    `json:"total_price"`
	NmID        int    `json:"nm_id"`
	Brand       string `json:"brand"`
	Status      int    `json:"status"`
}

func (i *Item) Validate() error {
	if i.Sale < 0 || i.Sale > 100 {
		return errs.NewInvalidOrderError("sale")
	}
	if i.Status != 201 && i.Status != 202 && i.Status != 402 {
		return errs.NewInvalidOrderError("unknown status")
	}
	return nil
}

package models

import (
	"time"

	"gitlab.com/triste/wbtech-l0/internal/errs"
	"gitlab.com/triste/wbtech-l0/pkg/validator"
)

type Order struct {
	ID                string    `json:"order_uid"`
	TrackNumber       string    `json:"track_number"`
	Entry             string    `json:"entry"`
	Delivery          Delivery  `json:"delivery"`
	Payment           Payment   `json:"payment"`
	Items             []Item    `json:"items"`
	Locale            string    `json:"locale"`
	InternalSignature string    `json:"internal_signature" validator:"allowempty"`
	CustomerID        string    `json:"customer_id"`
	DeliveryService   string    `json:"delivery_service"`
	Shardkey          string    `json:"shardkey"`
	SmID              int       `json:"sm_id"`
	DateCreated       time.Time `json:"date_created"`
	OofShard          string    `json:"oof_shard"`
}

func (o *Order) Validate() error {
	if err := validator.Validate(*o); err != nil {
		return err
	}
	if len(o.ID) != 19 {
		return errs.NewInvalidOrderError("id")
	}
	if err := o.Delivery.Validate(); err != nil {
		return err
	}
	if err := o.Payment.Validate(); err != nil {
		return err
	}
	if len(o.Items) == 0 {
		return errs.NewInvalidOrderError("zero items")
	}
	for _, item := range o.Items {
		if err := item.Validate(); err != nil {
			return err
		}
		if item.TrackNumber != o.TrackNumber {
			return errs.NewInvalidOrderError("track number doesn't match")
		}
	}
	if o.Locale != "en" && o.Locale != "ru" {
		return errs.NewInvalidOrderError("locale")
	}
	if o.DateCreated.IsZero() {
		return errs.NewInvalidOrderError("time is zero")
	}
	return nil
}

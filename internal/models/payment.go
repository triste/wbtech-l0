package models

import (
	"gitlab.com/triste/wbtech-l0/internal/errs"
)

type Payment struct {
	Transaction  string `json:"transaction"`
	RequestID    string `json:"request_id" validator:"allowempty"`
	Currency     string `json:"currency"`
	Provider     string `json:"provider"`
	Amount       int    `json:"amount"`
	PaymentDt    int    `json:"payment_dt"`
	Bank         string `json:"bank"`
	DeliveryCost int    `json:"delivery_cost"`
	GoodsTotal   int    `json:"goods_total"`
	CustomFee    int    `json:"custom_fee"`
}

func (p *Payment) Validate() error {
	if p.Currency != "USD" && p.Currency != "RUB" {
		return errs.NewInvalidOrderError("unknown currency")
	}
	return nil
}

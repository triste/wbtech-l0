package postgres

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"

	"github.com/lib/pq"
	"gitlab.com/triste/wbtech-l0/internal/errs"
	"gitlab.com/triste/wbtech-l0/internal/models"
	"go.uber.org/zap"
)

type OrderRepository struct {
	db     *sql.DB
	logger *zap.Logger
}

func NewOrderRepository(logger *zap.Logger, db *sql.DB) *OrderRepository {
	return &OrderRepository{
		db:     db,
		logger: logger,
	}
}

func (s *OrderRepository) GetAll(ctx context.Context) ([]models.Order, error) {
	const query = `SELECT id, data FROM orders`
	rows, err := s.db.Query(query)
	if err != nil {
		s.logger.Panic("select failed",
			zap.String("query", query),
			zap.Error(err),
		)
		return nil, err
	}
	defer rows.Close()
	var orders []models.Order
	for rows.Next() {
		var (
			id        string
			orderJson []byte
		)
		if err := rows.Scan(&id, &orderJson); err != nil {
			s.logger.Panic("row scan failed",
				zap.Error(err),
			)
			return nil, err
		}
		var order models.Order
		if err := json.Unmarshal(orderJson, &order); err != nil {
			s.logger.Panic("order unmarshal failed",
				zap.ByteString("order", orderJson),
				zap.Error(err),
			)
			return nil, err
		}
		orders = append(orders, order)
	}
	return orders, nil
}

func (s *OrderRepository) Add(ctx context.Context, order *models.Order) error {
	const query = `
	INSERT INTO orders(id, data)
	VALUES($1, $2)
	`
	orderJson, err := json.Marshal(order)
	if err != nil {
		s.logger.Panic("order mashal failed",
			zap.Any("order", order),
			zap.Error(err),
		)
	}
	if _, err := s.db.Exec(query, order.ID, orderJson); err != nil {
		var perr *pq.Error
		if errors.As(err, &perr) && perr.Code == "23505" {
			return errs.NewAlreadyExistsError("order with id %v", order.ID)
		}
		s.logger.Panic("insertion failed",
			zap.String("query", query),
			zap.Error(err),
		)
	}
	return nil
}

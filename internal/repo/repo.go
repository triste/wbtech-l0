package repo

import (
	"context"

	"gitlab.com/triste/wbtech-l0/internal/models"
)

type OrderRepository interface {
	GetAll(ctx context.Context) ([]models.Order, error)
	Add(ctx context.Context, order *models.Order) error
}

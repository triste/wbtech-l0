package service

import (
	"context"

	"gitlab.com/triste/wbtech-l0/internal/cache"
	"gitlab.com/triste/wbtech-l0/internal/models"
	"gitlab.com/triste/wbtech-l0/internal/repo"
	"go.uber.org/zap"
)

type Service struct {
	logger     *zap.Logger
	orderRepo  repo.OrderRepository
	orderCache cache.OrderCache
}

func (s *Service) GetOrder(ctx context.Context, id string) (*models.Order, error) {
	order, err := s.orderCache.GetByID(ctx, id)
	return order, err
}

func (s *Service) AddOrder(ctx context.Context, order *models.Order) error {
	if err := order.Validate(); err != nil {
		return err
	}
	if err := s.orderRepo.Add(ctx, order); err != nil {
		return err
	}
	if err := s.orderCache.Add(ctx, order); err != nil {
		s.logger.Warn("order caching failed",
			zap.Error(err),
		)
		return err
	}
	return nil
}

func NewService(
	logger *zap.Logger,
	orderRepo repo.OrderRepository,
	orderCache cache.OrderCache,
) Service {
	return Service{
		logger:     logger,
		orderRepo:  orderRepo,
		orderCache: orderCache,
	}
}

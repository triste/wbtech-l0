package validator

import (
	"fmt"
	"net/mail"
	"reflect"
	"strings"
)

func Validate(model any) error {
	modelVal := reflect.ValueOf(model)
	modelType := reflect.TypeOf(model)
	for i := 0; i < modelVal.NumField(); i++ {
		val := modelVal.Field(i)
		if val.Kind() == reflect.Struct {
			if err := Validate(val.Interface()); err != nil {
				return err
			}
		}
		typ := modelType.Field(i)
		validator := typ.Tag.Get("validator")
		if len(validator) == 0 {
			continue
		}
		opts := strings.Split(validator, ",")
		var (
			allowEmpty bool
			email      bool
		)
		for _, opt := range opts {
			switch opt {
			case "allowempty":
				allowEmpty = true
			case "email":
				email = true
			}
		}
		if val.Kind() == reflect.String {
			if val.Len() == 0 {
				if allowEmpty {
					continue
				}
				return fmt.Errorf("%v missed", typ.Name)
			}
			if email {
				if _, err := mail.ParseAddress(val.Interface().(string)); err != nil {
					return fmt.Errorf("%v invalid: %w", typ.Name, err)
				}
			}
		}
	}
	return nil
}
